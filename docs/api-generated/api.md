# Protocol Documentation
<a name="top"/>

## Table of Contents

- [api.proto](#api.proto)
    - [CreateFunctionMsg](#klessapi.CreateFunctionMsg)
    - [CreateFunctionMsg.HTTPTriggerMsg](#klessapi.CreateFunctionMsg.HTTPTriggerMsg)
    - [DeleteFunctionMsg](#klessapi.DeleteFunctionMsg)
    - [FunctionMsg](#klessapi.FunctionMsg)
    - [GenericReply](#klessapi.GenericReply)
    - [GetEngineListMsg](#klessapi.GetEngineListMsg)
    - [GetEngineListReply](#klessapi.GetEngineListReply)
    - [GetFunctionListMsg](#klessapi.GetFunctionListMsg)
    - [GetFunctionListReply](#klessapi.GetFunctionListReply)
    - [GetFunctionListReply.FunctionListEntry](#klessapi.GetFunctionListReply.FunctionListEntry)
    - [GetFunctionMsg](#klessapi.GetFunctionMsg)
    - [GetFunctionReply](#klessapi.GetFunctionReply)
    - [GetVersionMsg](#klessapi.GetVersionMsg)
    - [GetVersionReply](#klessapi.GetVersionReply)
    - [GetVersionReply.VersionInformation](#klessapi.GetVersionReply.VersionInformation)
    - [StreamPodLogMsg](#klessapi.StreamPodLogMsg)
    - [StreamPodLogReply](#klessapi.StreamPodLogReply)
    - [UpdateFunctionMsg](#klessapi.UpdateFunctionMsg)
  
  
  
    - [KlessAPI](#klessapi.KlessAPI)
  

- [api.proto](#api.proto)
    - [CreateFunctionMsg](#klessapi.CreateFunctionMsg)
    - [CreateFunctionMsg.HTTPTriggerMsg](#klessapi.CreateFunctionMsg.HTTPTriggerMsg)
    - [DeleteFunctionMsg](#klessapi.DeleteFunctionMsg)
    - [FunctionMsg](#klessapi.FunctionMsg)
    - [GenericReply](#klessapi.GenericReply)
    - [GetEngineListMsg](#klessapi.GetEngineListMsg)
    - [GetEngineListReply](#klessapi.GetEngineListReply)
    - [GetFunctionListMsg](#klessapi.GetFunctionListMsg)
    - [GetFunctionListReply](#klessapi.GetFunctionListReply)
    - [GetFunctionListReply.FunctionListEntry](#klessapi.GetFunctionListReply.FunctionListEntry)
    - [GetFunctionMsg](#klessapi.GetFunctionMsg)
    - [GetFunctionReply](#klessapi.GetFunctionReply)
    - [GetVersionMsg](#klessapi.GetVersionMsg)
    - [GetVersionReply](#klessapi.GetVersionReply)
    - [GetVersionReply.VersionInformation](#klessapi.GetVersionReply.VersionInformation)
    - [StreamPodLogMsg](#klessapi.StreamPodLogMsg)
    - [StreamPodLogReply](#klessapi.StreamPodLogReply)
    - [UpdateFunctionMsg](#klessapi.UpdateFunctionMsg)
  
  
  
    - [KlessAPI](#klessapi.KlessAPI)
  

- [Scalar Value Types](#scalar-value-types)



<a name="api.proto"/>
<p align="right"><a href="#top">Top</a></p>

## api.proto



<a name="klessapi.CreateFunctionMsg"/>

### CreateFunctionMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | The name of the function |
| function | [FunctionMsg](#klessapi.FunctionMsg) |  | Function parameters |
| http_trigger | [CreateFunctionMsg.HTTPTriggerMsg](#klessapi.CreateFunctionMsg.HTTPTriggerMsg) |  | HTTP Trigger parameters |






<a name="klessapi.CreateFunctionMsg.HTTPTriggerMsg"/>

### CreateFunctionMsg.HTTPTriggerMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| gateway | [string](#string) |  | What gateway to use (will default to nginx) |
| hostname | [string](#string) |  | Any hostname override (will default to name.namespace.clusterFQDN) |
| tls | [bool](#bool) |  | Whether or not TLS Acme should be used (defaults to no) |
| tls_secret | [string](#string) |  | What secret name should be used if TLS is to be used |






<a name="klessapi.DeleteFunctionMsg"/>

### DeleteFunctionMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | The name of the function |






<a name="klessapi.FunctionMsg"/>

### FunctionMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| deps | [string](#string) |  | What deps are needed for function |
| function | [string](#string) |  | The actual function contents |
| handler | [string](#string) |  | The handler to call (what should be called) |
| runtime | [string](#string) |  | The runtime to use |






<a name="klessapi.GenericReply"/>

### GenericReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | If it was successful |






<a name="klessapi.GetEngineListMsg"/>

### GetEngineListMsg







<a name="klessapi.GetEngineListReply"/>

### GetEngineListReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| engines | [string](#string) | repeated | List of engines |






<a name="klessapi.GetFunctionListMsg"/>

### GetFunctionListMsg







<a name="klessapi.GetFunctionListReply"/>

### GetFunctionListReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | Whether or not the request was OK |
| entries | [GetFunctionListReply.FunctionListEntry](#klessapi.GetFunctionListReply.FunctionListEntry) | repeated | List of functions |






<a name="klessapi.GetFunctionListReply.FunctionListEntry"/>

### GetFunctionListReply.FunctionListEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | Name of the function |
| runtime | [string](#string) |  | Name of the runtime |
| url | [GetFunctionListReply.FunctionListEntry.FunctionListURL](#klessapi.GetFunctionListReply.FunctionListEntry.FunctionListURL) |  | URL Information |
| deployment | [GetFunctionListReply.FunctionListEntry.FunctionListDeployment](#klessapi.GetFunctionListReply.FunctionListEntry.FunctionListDeployment) |  | Deployment Information |
| pod_status | [GetFunctionListReply.FunctionListEntry.FunctionListPodStatus](#klessapi.GetFunctionListReply.FunctionListEntry.FunctionListPodStatus) | repeated | Individual Pod Status |






<a name="klessapi.GetFunctionMsg"/>

### GetFunctionMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | The name of the function |






<a name="klessapi.GetFunctionReply"/>

### GetFunctionReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| checksum | [string](#string) |  | The checksum of the code |
| deps | [string](#string) |  | The dependencies of the function |
| function | [string](#string) |  | The actual function code |
| function_content_type | [string](#string) |  | The content type of the function (string) |
| handler | [string](#string) |  | The handler of the function (the actual function name in code) |
| name | [string](#string) |  | The name of the function |
| ok | [bool](#bool) |  | If fetching the data was successful |
| runtime | [string](#string) |  | The runtime of the function (go1.10/nodejs6/php72/etc.) |
| timeout | [string](#string) |  | The timeout for any function to run |






<a name="klessapi.GetVersionMsg"/>

### GetVersionMsg







<a name="klessapi.GetVersionReply"/>

### GetVersionReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | If operation was OK |
| version_information | [GetVersionReply.VersionInformation](#klessapi.GetVersionReply.VersionInformation) |  | Version Information |






<a name="klessapi.GetVersionReply.VersionInformation"/>

### GetVersionReply.VersionInformation



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| git_version | [string](#string) |  | The tag on the git repository |
| git_commit | [string](#string) |  | The hash of the git commit |
| git_tree_state | [string](#string) |  | Whether or not the tree was clean when built |
| build_date | [string](#string) |  | Date of build |
| go_version | [string](#string) |  | Version of go used to compile |
| compiler | [string](#string) |  | Compiler used |
| platform | [string](#string) |  | Platform it was compiled for / running on |






<a name="klessapi.StreamPodLogMsg"/>

### StreamPodLogMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | Name of the pod |
| container | [string](#string) |  | Name of the container in the pod |
| namespace | [string](#string) |  | Namespace of the pod |






<a name="klessapi.StreamPodLogReply"/>

### StreamPodLogReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| log | [bytes](#bytes) |  | Contents |






<a name="klessapi.UpdateFunctionMsg"/>

### UpdateFunctionMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | The name of the function |
| function | [FunctionMsg](#klessapi.FunctionMsg) |  | Function parameters |





 

 

 


<a name="klessapi.KlessAPI"/>

### KlessAPI


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| GetVersionInformation | [GetVersionMsg](#klessapi.GetVersionMsg) | [GetVersionReply](#klessapi.GetVersionMsg) | Will return version information about api server |
| GetFunction | [GetFunctionMsg](#klessapi.GetFunctionMsg) | [GetFunctionReply](#klessapi.GetFunctionMsg) | Will get a function&#39;s details |
| CreateFunction | [CreateFunctionMsg](#klessapi.CreateFunctionMsg) | [GenericReply](#klessapi.CreateFunctionMsg) | Will create a function |
| UpdateFunction | [UpdateFunctionMsg](#klessapi.UpdateFunctionMsg) | [GenericReply](#klessapi.UpdateFunctionMsg) | Will update a function |
| DeleteFunction | [DeleteFunctionMsg](#klessapi.DeleteFunctionMsg) | [GenericReply](#klessapi.DeleteFunctionMsg) | Will Delete a function |
| GetFunctionList | [GetFunctionListMsg](#klessapi.GetFunctionListMsg) | [GetFunctionListReply](#klessapi.GetFunctionListMsg) | Will return the function list (and status) |
| GetEngineList | [GetEngineListMsg](#klessapi.GetEngineListMsg) | [GetEngineListReply](#klessapi.GetEngineListMsg) | Will list available engines |
| StreamPodLog | [StreamPodLogMsg](#klessapi.StreamPodLogMsg) | [StreamPodLogReply](#klessapi.StreamPodLogMsg) | Will stream logs |

 



<a name="api.proto"/>
<p align="right"><a href="#top">Top</a></p>

## api.proto



<a name="klessapi.CreateFunctionMsg"/>

### CreateFunctionMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | The name of the function |
| function | [FunctionMsg](#klessapi.FunctionMsg) |  | Function parameters |
| http_trigger | [CreateFunctionMsg.HTTPTriggerMsg](#klessapi.CreateFunctionMsg.HTTPTriggerMsg) |  | HTTP Trigger parameters |






<a name="klessapi.CreateFunctionMsg.HTTPTriggerMsg"/>

### CreateFunctionMsg.HTTPTriggerMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| gateway | [string](#string) |  | What gateway to use (will default to nginx) |
| hostname | [string](#string) |  | Any hostname override (will default to name.namespace.clusterFQDN) |
| tls | [bool](#bool) |  | Whether or not TLS Acme should be used (defaults to no) |
| tls_secret | [string](#string) |  | What secret name should be used if TLS is to be used |






<a name="klessapi.DeleteFunctionMsg"/>

### DeleteFunctionMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | The name of the function |






<a name="klessapi.FunctionMsg"/>

### FunctionMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| deps | [string](#string) |  | What deps are needed for function |
| function | [string](#string) |  | The actual function contents |
| handler | [string](#string) |  | The handler to call (what should be called) |
| runtime | [string](#string) |  | The runtime to use |






<a name="klessapi.GenericReply"/>

### GenericReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | If it was successful |






<a name="klessapi.GetEngineListMsg"/>

### GetEngineListMsg







<a name="klessapi.GetEngineListReply"/>

### GetEngineListReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| engines | [string](#string) | repeated | List of engines |






<a name="klessapi.GetFunctionListMsg"/>

### GetFunctionListMsg







<a name="klessapi.GetFunctionListReply"/>

### GetFunctionListReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | Whether or not the request was OK |
| entries | [GetFunctionListReply.FunctionListEntry](#klessapi.GetFunctionListReply.FunctionListEntry) | repeated | List of functions |






<a name="klessapi.GetFunctionListReply.FunctionListEntry"/>

### GetFunctionListReply.FunctionListEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | Name of the function |
| runtime | [string](#string) |  | Name of the runtime |
| url | [GetFunctionListReply.FunctionListEntry.FunctionListURL](#klessapi.GetFunctionListReply.FunctionListEntry.FunctionListURL) |  | URL Information |
| deployment | [GetFunctionListReply.FunctionListEntry.FunctionListDeployment](#klessapi.GetFunctionListReply.FunctionListEntry.FunctionListDeployment) |  | Deployment Information |
| pod_status | [GetFunctionListReply.FunctionListEntry.FunctionListPodStatus](#klessapi.GetFunctionListReply.FunctionListEntry.FunctionListPodStatus) | repeated | Individual Pod Status |






<a name="klessapi.GetFunctionMsg"/>

### GetFunctionMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | The name of the function |






<a name="klessapi.GetFunctionReply"/>

### GetFunctionReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| checksum | [string](#string) |  | The checksum of the code |
| deps | [string](#string) |  | The dependencies of the function |
| function | [string](#string) |  | The actual function code |
| function_content_type | [string](#string) |  | The content type of the function (string) |
| handler | [string](#string) |  | The handler of the function (the actual function name in code) |
| name | [string](#string) |  | The name of the function |
| ok | [bool](#bool) |  | If fetching the data was successful |
| runtime | [string](#string) |  | The runtime of the function (go1.10/nodejs6/php72/etc.) |
| timeout | [string](#string) |  | The timeout for any function to run |






<a name="klessapi.GetVersionMsg"/>

### GetVersionMsg







<a name="klessapi.GetVersionReply"/>

### GetVersionReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | If operation was OK |
| version_information | [GetVersionReply.VersionInformation](#klessapi.GetVersionReply.VersionInformation) |  | Version Information |






<a name="klessapi.GetVersionReply.VersionInformation"/>

### GetVersionReply.VersionInformation



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| git_version | [string](#string) |  | The tag on the git repository |
| git_commit | [string](#string) |  | The hash of the git commit |
| git_tree_state | [string](#string) |  | Whether or not the tree was clean when built |
| build_date | [string](#string) |  | Date of build |
| go_version | [string](#string) |  | Version of go used to compile |
| compiler | [string](#string) |  | Compiler used |
| platform | [string](#string) |  | Platform it was compiled for / running on |






<a name="klessapi.StreamPodLogMsg"/>

### StreamPodLogMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | Name of the pod |
| container | [string](#string) |  | Name of the container in the pod |
| namespace | [string](#string) |  | Namespace of the pod |






<a name="klessapi.StreamPodLogReply"/>

### StreamPodLogReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| log | [bytes](#bytes) |  | Contents |






<a name="klessapi.UpdateFunctionMsg"/>

### UpdateFunctionMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | The name of the function |
| function | [FunctionMsg](#klessapi.FunctionMsg) |  | Function parameters |





 

 

 


<a name="klessapi.KlessAPI"/>

### KlessAPI


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| GetVersionInformation | [GetVersionMsg](#klessapi.GetVersionMsg) | [GetVersionReply](#klessapi.GetVersionMsg) | Will return version information about api server |
| GetFunction | [GetFunctionMsg](#klessapi.GetFunctionMsg) | [GetFunctionReply](#klessapi.GetFunctionMsg) | Will get a function&#39;s details |
| CreateFunction | [CreateFunctionMsg](#klessapi.CreateFunctionMsg) | [GenericReply](#klessapi.CreateFunctionMsg) | Will create a function |
| UpdateFunction | [UpdateFunctionMsg](#klessapi.UpdateFunctionMsg) | [GenericReply](#klessapi.UpdateFunctionMsg) | Will update a function |
| DeleteFunction | [DeleteFunctionMsg](#klessapi.DeleteFunctionMsg) | [GenericReply](#klessapi.DeleteFunctionMsg) | Will Delete a function |
| GetFunctionList | [GetFunctionListMsg](#klessapi.GetFunctionListMsg) | [GetFunctionListReply](#klessapi.GetFunctionListMsg) | Will return the function list (and status) |
| GetEngineList | [GetEngineListMsg](#klessapi.GetEngineListMsg) | [GetEngineListReply](#klessapi.GetEngineListMsg) | Will list available engines |
| StreamPodLog | [StreamPodLogMsg](#klessapi.StreamPodLogMsg) | [StreamPodLogReply](#klessapi.StreamPodLogMsg) | Will stream logs |

 



## Scalar Value Types

| .proto Type | Notes | C++ Type | Java Type | Python Type |
| ----------- | ----- | -------- | --------- | ----------- |
| <a name="double" /> double |  | double | double | float |
| <a name="float" /> float |  | float | float | float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long |
| <a name="bool" /> bool |  | bool | boolean | boolean |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str |

