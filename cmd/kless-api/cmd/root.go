package cmd

import (
	"flag"
	"fmt"
	"github.com/juju/loggo"
	"github.com/soheilhy/cmux"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mvenezia/kless-api/pkg/apiserver"
	"gitlab.com/mvenezia/kless-api/pkg/listers"
	"gitlab.com/mvenezia/kless-api/pkg/util/k8sutil"
	logutil "gitlab.com/mvenezia/kless-api/pkg/util/log"
	"net"
	"os"
	"strings"
	"sync"
)

var (
	rootCmd = &cobra.Command{
		Use:   "kless-api",
		Short: "Kubeless helper API",
		Long:  `The Kubeless helper API`,
		Run: func(cmd *cobra.Command, args []string) {
			runWebServer()
		},
	}
)

func init() {
	viper.SetEnvPrefix("kless")
	replacer := strings.NewReplacer("-", "_")
	viper.SetEnvKeyReplacer(replacer)

	// using standard library "flag" package
	rootCmd.Flags().Int("port", 9050, "Port to listen on")
	rootCmd.Flags().String("kubeconfig", "", "Location of kubeconfig file")
	rootCmd.Flags().String("kubectl", "kubectl", "Location of kubectl file")
	rootCmd.Flags().String("kubernetes-namespace", "kubeless", "What namespace to operate on")

	viper.BindPFlag("port", rootCmd.Flags().Lookup("port"))
	viper.BindPFlag("kubeconfig", rootCmd.Flags().Lookup("kubeconfig"))
	viper.BindPFlag("kubectl", rootCmd.Flags().Lookup("kubectl"))
	viper.BindPFlag("kubernetes-namespace", rootCmd.Flags().Lookup("kubernetes-namespace"))

	viper.AutomaticEnv()
	rootCmd.Flags().AddGoFlagSet(flag.CommandLine)
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func runWebServer() {
	var err error
	logger := logutil.GetModuleLogger("cmd.cluster-manager-api", loggo.INFO)

	// get flags
	portNumber := viper.GetInt("port")
	kubeconfigLocation := viper.GetString("kubeconfig")

	// Debug for now
	logger.Infof("Parsed Variables: \n  Port: %d\n  Kubeconfig: %s\n  kubectl: %s\n  Kubernetes Namespace: %s\n", portNumber, kubeconfigLocation, viper.GetString("kubectl"), viper.GetString("kubernetes-namespace"))

	k8sutil.KubeConfigLocation = kubeconfigLocation
	k8sutil.DefaultConfig, err = k8sutil.GenerateKubernetesConfig()

	if err != nil {
		logger.Infof("Was unable to generate a valid kubernetes default config, some functionality may be broken.  Error was %v", err)
	}

	listers.StartListWatchers()

	var wg sync.WaitGroup
	stop := make(chan struct{})

	logger.Infof("Creating Web Server")
	tcpMux := createWebServer(&apiserver.ServerOptions{PortNumber: portNumber})
	wg.Add(1)
	go func() {
		defer wg.Done()
		logger.Infof("Starting to serve requests on port %d", portNumber)
		tcpMux.Serve()
	}()

	<-stop
	logger.Infof("Wating for controllers to shut down gracefully")
	wg.Wait()
}

func createWebServer(options *apiserver.ServerOptions) cmux.CMux {
	conn, err := net.Listen("tcp", fmt.Sprintf(":%d", options.PortNumber))
	if err != nil {
		panic(err)
	}
	tcpMux := cmux.New(conn)

	apiserver.AddServersToMux(tcpMux, options)

	return tcpMux
}
