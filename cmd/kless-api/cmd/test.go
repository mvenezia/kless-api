package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/kless-api/pkg/kubeless"
	"gitlab.com/mvenezia/kless-api/pkg/util/k8sutil"
	"io"
	"k8s.io/api/authorization/v1"
	podv1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
	"time"
)

var (
	testCmd = &cobra.Command{
		Use:   "test",
		Short: "Test Command",
		Long:  `For Testing purposes`,
		Run: func(cmd *cobra.Command, args []string) {
			doTestStuff()
		},
	}
)

func init() {
	rootCmd.AddCommand(testCmd)
}

func doTestStuff() {
	k8sutil.DefaultConfig, _ = k8sutil.GenerateKubernetesConfig()
	client, err := kubernetes.NewForConfig(k8sutil.DefaultConfig)
	if err != nil {
		fmt.Printf("Error was %s\n", err)
		return
	}

	resource := "pods"
	verb := "get, create, list"
	namespace := "kubeless"
	group := "v1"

	accessReview := &v1.SelfSubjectAccessReview{
		Spec: v1.SelfSubjectAccessReviewSpec{
			ResourceAttributes: &v1.ResourceAttributes{
				Group:     group,
				Resource:  resource,
				Verb:      verb,
				Namespace: namespace,
			},
		},
	}
	answer, err := client.AuthorizationV1().SelfSubjectAccessReviews().Create(accessReview)
	if err != nil {
		fmt.Printf("Error was %s\n", err)
		return
	}
	fmt.Printf("Can the program %s %s/%s in the %s namespace?\n", verb, group, resource, namespace)
	fmt.Printf("Answer: %v\nReason: %s\n", answer.Status.Allowed, answer.Status.Reason)
	log := client.CoreV1().Pods("kubeless").GetLogs("kless-ui-945874848-l4rwq", &podv1.PodLogOptions{Follow: true})
	readCloser, err := log.Stream()
	if err != nil {
		fmt.Printf("Error was %s\n", err)
	}
	//defer readCloser.Close()
	boom := time.After(3 * time.Second)
	boom2 := time.After(4 * time.Second)
	//for {
	//	select {
	//	case <- boom:
	//		fmt.Println("Closing stream as 10 seconds has expired")
	//		return
	//	default:
	//		p := make([]byte, 4096)
	//		n, err := readCloser.Read(p)
	//		if err != nil {
	//			if err == io.EOF {
	//				fmt.Println("Upstream shut down")
	//				return
	//			}
	//		}
	//		fmt.Println(string(p[:n]))
	//	}
	//}
	input := make(chan string, 1)
	control := make(chan string, 1)
	go streamLog(input, readCloser, control)
	for {
		select {
		case i := <-input:
			fmt.Println(i)
		case <-boom:
			fmt.Println("timed out")
			control <- "die"
			readCloser.Close()
		case <-boom2:
			fmt.Println("closing out")
			return
		}
	}
}

func streamLog(input chan string, reader io.Reader, control <-chan string) {
	p := make([]byte, 4096)
	for {
		select {
		case _ = <-control:
			fmt.Println("Got message to die")
			return
		default:
			n, err := reader.Read(p)
			if err != nil {
				if err == io.EOF {
					fmt.Println("Upstream shut down")
					return
				}
			}
			input <- string(p[:n])
		}
	}
}

func doTestStuffs() {
	functions, err := kubeless.GetFunctionStatus()
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		return
	}
	fmt.Println("Functions installed:\n")
	for _, item := range functions {
		url := "Not Set"
		if item.URL.Exists {
			url = item.URL.URL
		}

		fmt.Printf("\t%s\n", item.Name)
		fmt.Printf("\t\tURL: %s\n", url)
		fmt.Printf("\t\tDeployment:\n")
		fmt.Printf("\t\t\tGeneration: %d\n", item.Deployment.Generation)
		fmt.Printf("\t\t\tReplicas: %d\n", item.Deployment.Replicas)
		fmt.Printf("\t\t\tUpdated: %d\n", item.Deployment.Updated)
		fmt.Printf("\t\t\tAvailable: %d\n", item.Deployment.Available)
		fmt.Printf("\t\t\tReady: %d\n", item.Deployment.Ready)
		fmt.Printf("\t\tIndividual Pod Status:\n")
		for _, pod := range item.Pods {
			ready := "Not Ready"
			if pod.Ready {
				ready = "Ready"
			}
			fmt.Printf("\t\t\tName: %s, Status: %s\n", pod.Name, ready)
		}
		fmt.Printf("\n")
	}
}
