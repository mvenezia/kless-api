package klessapi

import (
	"github.com/juju/loggo"
	"gitlab.com/mvenezia/kless-api/pkg/util/log"
)

var (
	logger loggo.Logger
)

type Server struct{}

func SetLogger() {
	logger = log.GetModuleLogger("internal.cluster-manager-api", loggo.INFO)
}
