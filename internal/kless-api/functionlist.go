package klessapi

import (
	pb "gitlab.com/mvenezia/kless-api/pkg/generated/api"
	"golang.org/x/net/context"

	"gitlab.com/mvenezia/kless-api/pkg/kubeless"
)

func (s *Server) GetFunctionList(ctx context.Context, in *pb.GetFunctionListMsg) (*pb.GetFunctionListReply, error) {
	SetLogger()

	output := &pb.GetFunctionListReply{Ok: true}

	functionList, err := kubeless.GetFunctionStatus()
	if err != nil {
		return nil, err
	}

	for _, item := range functionList {
		entry := &pb.GetFunctionListReply_FunctionListEntry{
			Name:    item.Name,
			Runtime: item.Runtime,
			Url: &pb.GetFunctionListReply_FunctionListEntry_FunctionListURL{
				Set: item.URL.Exists,
				Url: item.URL.URL,
			},
			Deployment: &pb.GetFunctionListReply_FunctionListEntry_FunctionListDeployment{
				Generation: item.Deployment.Generation,
				Replicas:   item.Deployment.Replicas,
				Updated:    item.Deployment.Updated,
				Available:  item.Deployment.Available,
				Ready:      item.Deployment.Ready,
			},
		}
		for _, pod := range item.Pods {
			entry.PodStatus = append(entry.PodStatus, &pb.GetFunctionListReply_FunctionListEntry_FunctionListPodStatus{
				Name:  pod.Name,
				Ready: pod.Ready,
			})
		}

		output.Entries = append(output.Entries, entry)
	}

	return output, nil
}
