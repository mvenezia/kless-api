package klessapi

import (
	pb "gitlab.com/mvenezia/kless-api/pkg/generated/api"
	"golang.org/x/net/context"

	"gitlab.com/mvenezia/kless-api/pkg/kubeless"
)

func (s *Server) GetEngineList(ctx context.Context, in *pb.GetEngineListMsg) (*pb.GetEngineListReply, error) {
	SetLogger()

	runtimeVersions, err := kubeless.GetAvailableRuntimes()

	if err != nil {
		return nil, err
	}

	return &pb.GetEngineListReply{Engines: runtimeVersions}, nil
}
