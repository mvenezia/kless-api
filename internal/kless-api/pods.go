package klessapi

import (
	pb "gitlab.com/mvenezia/kless-api/pkg/generated/api"

	"encoding/json"
	"github.com/spf13/viper"
	"gitlab.com/mvenezia/kless-api/pkg/util/k8sutil"
)

func (s *Server) StreamPodLog(in *pb.StreamPodLogMsg, stream pb.KlessAPI_StreamPodLogServer) (err error) {
	SetLogger()

	input := make(chan string)
	control := make(chan string)

	err = k8sutil.GetLogsForPod(viper.GetString("kubernetes-namespace"), in.Name, input, control)
	if err != nil {
		return
	}
	defer func() { control <- "die" }()

	for {
		select {
		case logMessage := <-input:
			messageBytes, _ := json.Marshal(logMessage)
			if err = stream.Send(&pb.StreamPodLogReply{Log: messageBytes}); err != nil {
				control <- "die"
				return err
			}
		}
	}

	return
}
