package klessapi

import (
	pb "gitlab.com/mvenezia/kless-api/pkg/generated/api"
	"golang.org/x/net/context"

	"errors"
	"github.com/spf13/viper"
	"gitlab.com/mvenezia/kless-api/pkg/kubeless"
)

const HTTPTriggerDefaultGateway = "nginx"
const HTTPTriggerTLS = false

func (s *Server) GetFunction(ctx context.Context, in *pb.GetFunctionMsg) (*pb.GetFunctionReply, error) {
	SetLogger()

	// Let's first create the function
	function, err := kubeless.GetFunction(in.Name)
	if err != nil {
		return &pb.GetFunctionReply{Ok: false, Name: in.Name}, err
	}

	return &pb.GetFunctionReply{
		Checksum:            function.Checksum,
		Deps:                function.Deps,
		Function:            function.Function,
		FunctionContentType: function.FunctionContentType,
		Handler:             function.Handler,
		Name:                in.Name,
		Ok:                  true,
		Runtime:             function.Runtime,
		Timeout:             function.Timeout,
	}, nil
}

func (s *Server) CreateFunction(ctx context.Context, in *pb.CreateFunctionMsg) (*pb.GenericReply, error) {
	SetLogger()

	// Let's first create the function
	_, err := kubeless.ApplyFunction(in.Name,
		viper.GetString("kubernetes-namespace"),
		in.Function.Runtime,
		in.Function.Function,
		in.Function.Handler,
		in.Function.Deps)
	if err != nil {
		return &pb.GenericReply{Ok: false}, err
	}

	gateway := HTTPTriggerDefaultGateway
	hostname := in.Name + ".kubeless.ontario.cluster.cnct.io"
	tls := HTTPTriggerTLS
	tlsSecret := "star-kubeless-ontario-tls"
	if in.HttpTrigger.Gateway != "" {
		gateway = in.HttpTrigger.Gateway
	}
	if in.HttpTrigger.Hostname != "" {
		hostname = in.HttpTrigger.Hostname
	}
	if in.HttpTrigger.Tls {
		tls = true
	}
	if in.HttpTrigger.TlsSecret != "" {
		tlsSecret = in.HttpTrigger.TlsSecret
	}

	_, err = kubeless.ApplyHTTPTrigger(in.Name,
		viper.GetString("kubernetes-namespace"),
		hostname,
		gateway,
		tls,
		tlsSecret)

	if err != nil {
		_, nestedError := kubeless.DeleteFunction(in.Name, viper.GetString("kubernetes-namespace"))
		if nestedError != nil {
			return &pb.GenericReply{Ok: false}, errors.New("Two errors occured! -->" + err.Error() + "<-- and -->" + nestedError.Error() + "%s<--")
		}
		return &pb.GenericReply{Ok: false}, err
	}

	return &pb.GenericReply{Ok: true}, nil
}

func (s *Server) UpdateFunction(ctx context.Context, in *pb.UpdateFunctionMsg) (*pb.GenericReply, error) {
	SetLogger()

	// Let's first create the function
	_, err := kubeless.ApplyFunction(in.Name,
		viper.GetString("kubernetes-namespace"),
		in.Function.Runtime,
		in.Function.Function,
		in.Function.Handler,
		in.Function.Deps)
	if err != nil {
		return &pb.GenericReply{Ok: false}, err
	}

	return &pb.GenericReply{Ok: true}, nil
}

func (s *Server) DeleteFunction(ctx context.Context, in *pb.DeleteFunctionMsg) (*pb.GenericReply, error) {
	SetLogger()
	_, err := kubeless.DeleteFunction(in.Name, viper.GetString("kubernetes-namespace"))
	if err != nil {
		return &pb.GenericReply{Ok: true}, err
	}

	return &pb.GenericReply{Ok: true}, nil
}
