package kubeless

import (
	"fmt"
	"gitlab.com/mvenezia/kless-api/pkg/kubeless/models"
	"gopkg.in/yaml.v2"
	"log"

	"crypto/sha256"

	kubelessclient "github.com/kubeless/kubeless/pkg/client/clientset/versioned"
	"github.com/spf13/viper"
	"gitlab.com/mvenezia/kless-api/pkg/util/k8sutil"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth/oidc"
)

const (
	DeployAPIVersion     = "apps/v1"
	DeployKind           = "Deployment"
	ReplicaSetAPIVersion = "extensions/v1beta1"
	ReplicaSetKind       = "ReplicaSet"
)

func generateHash(codeBytes []byte) string {
	hashFunction := sha256.New()
	hashFunction.Write(codeBytes)
	return "sha256:" + fmt.Sprintf("%x", hashFunction.Sum(nil))
}

func ApplyFunction(name string, namespace string, runtime string, code string, handler string, deps string) (string, error) {
	codeBytes := []byte(code)
	hash := generateHash(codeBytes)

	object := models.Function{
		APIVersion: models.APIVersion,
		Kind:       models.FunctionResourceKind,
		MetaData:   models.MetaData{Name: name, Namespace: namespace},
		Spec: models.FunctionSpec{
			Checksum:            hash,
			Deps:                deps,
			Function:            code,
			FunctionContentType: models.FunctionContentTypeText,
			Handler:             handler,
			Runtime:             runtime,
			Timeout:             models.FunctionDefaultTimeout,
		},
	}
	return ApplyFunctionDirect(object)
}

func ApplyFunctionDirect(function models.Function) (string, error) {
	// Generating the YAML...
	data, err := yaml.Marshal(&function)
	if err != nil {
		log.Printf("Error Converting Function to YAML, name was -->%s<--, error was: %s\n", function.MetaData.Name, err)
		return "", err
	}

	return applyYAML(data)
}

func DeleteFunction(name string, namespace string) (string, error) {
	return deleteResource(name, models.FunctionFQResourceName, namespace)
}

func GetFunctionStatus() (result map[string]models.FunctionStatus, err error) {
	k8sutil.DefaultConfig, _ = k8sutil.GenerateKubernetesConfig()
	kubelessclt, err := kubelessclient.NewForConfig(k8sutil.DefaultConfig)
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}

	functionList, err := kubelessclt.KubelessV1beta1().Functions(viper.GetString("kubernetes-namespace")).List(metav1.ListOptions{})
	httpTriggerList, err := kubelessclt.KubelessV1beta1().HTTPTriggers(viper.GetString("kubernetes-namespace")).List(metav1.ListOptions{})

	k8sclt, err := kubernetes.NewForConfig(k8sutil.DefaultConfig)
	deploymentList, err := k8sclt.ExtensionsV1beta1().Deployments(viper.GetString("kubernetes-namespace")).List(metav1.ListOptions{})
	podList, err := k8sclt.CoreV1().Pods(viper.GetString("kubernetes-namespace")).List(metav1.ListOptions{})

	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}

	result = make(map[string]models.FunctionStatus)
	for _, item := range functionList.Items {
		result[item.Name] = models.FunctionStatus{
			Name:       item.Name,
			Runtime:    item.Spec.Runtime,
			Deployment: &models.DeploymentStatus{},
			Pods:       make(map[string]models.PodStatus),
			URL:        &models.HTTPTriggerStatus{},
		}
	}

	for _, item := range podList.Items {
		if _, ok := result[item.Labels["function"]]; ok {
			podReady := false
			for _, statusItem := range item.Status.Conditions {
				if statusItem.Type == "Ready" {
					if statusItem.Status == v1.ConditionTrue {
						podReady = true
					}
					break
				}
			}
			result[item.Labels["function"]].Pods[item.Name] = models.PodStatus{Name: item.Name, Ready: podReady}
		}
	}

	for _, item := range deploymentList.Items {
		if _, ok := result[item.Labels["function"]]; ok {
			tempDeployment := result[item.Labels["function"]].Deployment
			tempDeployment.Available = item.Status.AvailableReplicas
			tempDeployment.Generation = int32(item.Status.ObservedGeneration)
			tempDeployment.Ready = item.Status.ReadyReplicas
			tempDeployment.Updated = item.Status.UpdatedReplicas
			tempDeployment.Replicas = item.Status.Replicas
		}
	}

	for _, item := range httpTriggerList.Items {
		if _, ok := result[item.Name]; ok {
			if item.Spec.HostName != "" {
				result[item.Name].URL.Exists = true
				if item.Spec.TLSSecret == "" {
					result[item.Name].URL.URL = "http://" + item.Spec.HostName
				} else {
					result[item.Name].URL.URL = "https://" + item.Spec.HostName
				}
			}
		}
	}

	return
}

func GetFunction(name string) (output models.FunctionSpec, err error) {
	kubelessclt, err := kubelessclient.NewForConfig(k8sutil.DefaultConfig)
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}

	function, err := kubelessclt.KubelessV1beta1().Functions(viper.GetString("kubernetes-namespace")).Get(name, metav1.GetOptions{})
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}
	output = models.FunctionSpec{
		Checksum:            function.Spec.FunctionContentType,
		Deps:                function.Spec.Deps,
		Function:            function.Spec.Function,
		FunctionContentType: function.Spec.FunctionContentType,
		Handler:             function.Spec.Handler,
		Runtime:             function.Spec.Runtime,
		Timeout:             function.Spec.Timeout,
	}
	return
}
