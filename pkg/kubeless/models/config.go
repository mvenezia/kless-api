package models

type LanguageVersion struct {
	Name         string `json:"name"`
	Version      string `json:"version"`
	RuntimeImage string `json:"runtimeImage"`
	InitImage    string `json:"initImage"`
}

type RunTimeImages struct {
	ID             string            `json:"ID"`
	Compiled       bool              `json:"compiled"`
	Versions       []LanguageVersion `json:"versions"`
	DepName        string            `json:"depName"`
	FileNameSuffix string            `json:"fileNameSuffix"`
}
