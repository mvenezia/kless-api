package models

type MetaData struct {
	Name      string `yaml:"name"`
	Namespace string `yaml:"namespace"`
}

const APIVersion = "kubeless.io/v1beta1"
