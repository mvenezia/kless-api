package models

type HTTPTriggerSpec struct {
	BasicAuthSecret string `yaml:"basic-auth-secret"`
	FunctionName    string `yaml:"function-name"`
	Gateway         string `yaml:"gateway"`
	HostName        string `yaml:"host-name"`
	Path            string `yaml:"path"`
	TLS             bool   `yaml:"tls"`
	TLSSecret       string `yaml:"tls-secret"`
}

type HTTPTrigger struct {
	Kind       string          `yaml:"kind"`
	APIVersion string          `yaml:"apiVersion"`
	MetaData   MetaData        `yaml:"metadata"`
	Spec       HTTPTriggerSpec `yaml:"spec"`
}

const HTTPTriggerResourceKind = "HTTPTrigger"
const HTTPTriggerFQResourceName = "httptrigger.kubeless.io"
