package models

type PodStatus struct {
	Name  string
	Ready bool
}

type DeploymentStatus struct {
	Available  int32
	Ready      int32
	Replicas   int32
	Updated    int32
	Generation int32
}

type HTTPTriggerStatus struct {
	Exists bool
	URL    string
}

type FunctionStatus struct {
	Name       string
	Runtime    string
	Pods       map[string]PodStatus
	Deployment *DeploymentStatus
	URL        *HTTPTriggerStatus
}
