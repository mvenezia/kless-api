package models

type FunctionSpec struct {
	Runtime             string `yaml:"runtime"`
	Timeout             string `yaml:"timeout"`
	Handler             string `yaml:"handler"`
	Deps                string `yaml:"deps"`
	Checksum            string `yaml:"checksum"`
	FunctionContentType string `yaml:"function-content-type"`
	Function            string `yaml:"function"`
}

type Function struct {
	Kind       string       `yaml:"kind"`
	APIVersion string       `yaml:"apiVersion"`
	MetaData   MetaData     `yaml:"metadata"`
	Spec       FunctionSpec `yaml:"spec"`
}

const FunctionContentTypeText = "text"
const FunctionDefaultTimeout = "180"

const FunctionResourceKind = "Function"
const FunctionFQResourceName = "function.kubeless.io"
