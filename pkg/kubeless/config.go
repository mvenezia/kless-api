package kubeless

import (
	"encoding/json"
	"github.com/spf13/viper"
	"gitlab.com/mvenezia/kless-api/pkg/kubeless/models"
	"log"
	"os/exec"
)

func getRuntimeImagesFromConfig() ([]byte, error) {
	cmdName := getKubectl()
	cmdArgs := []string{"get", "configmap/kubeless-config", "--namespace", viper.GetString("kubernetes-namespace"), "-o", "jsonpath='{.data.runtime-images}"}

	output, err := exec.Command(cmdName, cmdArgs...).Output()
	if err != nil {
		log.Printf("Error executing command: %s\n", err)
		return nil, err
	}
	return output[1:], nil
}

func GetAvailableRuntimes() ([]string, error) {
	runtimeConfigBytes, err := getRuntimeImagesFromConfig()
	if err != nil {
		return nil, err
	}

	var configuration []models.RunTimeImages
	err = json.Unmarshal(runtimeConfigBytes, &configuration)
	if err != nil {
		log.Printf("error: %s\n", err)
		return nil, err
	}

	var runtimeVersions []string
	for _, runtime := range configuration {
		for _, version := range runtime.Versions {
			found := false
			for _, versionValue := range runtimeVersions {
				if versionValue == runtime.ID+version.Version {
					found = true
					break
				}
			}
			if found == false {
				runtimeVersions = append(runtimeVersions, runtime.ID+version.Version)
			}
		}
	}

	return runtimeVersions, nil
}
