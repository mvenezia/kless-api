package kubeless

import (
	"gitlab.com/mvenezia/kless-api/pkg/kubeless/models"
	"gopkg.in/yaml.v2"
	"log"
)

func ApplyHTTPTrigger(name string, namespace string, hostname string, gateway string, tls bool, tlsSecret string) (string, error) {
	object := models.HTTPTrigger{
		APIVersion: models.APIVersion,
		Kind:       models.HTTPTriggerResourceKind,
		MetaData:   models.MetaData{Name: name, Namespace: namespace},
		Spec: models.HTTPTriggerSpec{
			FunctionName: name,
			Gateway:      gateway,
			HostName:     hostname,
			TLS:          tls,
			TLSSecret:    tlsSecret,
		},
	}
	return ApplyHTTPTriggerDirect(object)
}

func ApplyHTTPTriggerDirect(httptrigger models.HTTPTrigger) (string, error) {
	// Generating the YAML...
	data, err := yaml.Marshal(&httptrigger)
	if err != nil {
		log.Printf("Error Converting Function to YAML, name was -->%s<--, error was: %s\n", httptrigger.MetaData.Name, err)
		return "", err
	}

	return applyYAML(data)
}

func DeleteHTTPTrigger(name string, namespace string) (string, error) {
	return deleteResource(name, models.HTTPTriggerFQResourceName, namespace)
}
