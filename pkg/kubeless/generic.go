package kubeless

import (
	"fmt"
	"github.com/spf13/viper"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
)

const TempFilePrefix = "kless-kubeless-yaml"

func getKubectl() string {
	// TODO This should be customizable (environmental variable, otherwise, maybe?)
	return viper.GetString("kubectl")
}

func applyYAML(yaml []byte) (string, error) {
	// Creating the file and priming it for removal afterwards
	file, err := ioutil.TempFile(os.TempDir(), TempFilePrefix)
	if err != nil {
		log.Printf("Error Creating temporary file, error was: %s\n", err)
		return "", err
	}
	defer os.Remove(file.Name())

	_, err = file.Write(yaml)
	if err != nil {
		log.Printf("Error writing to file -->%s<--, error was: %s\n", file.Name(), err)
		return "", err
	}

	cmdName := getKubectl()
	cmdArgs := []string{"apply", "-f", file.Name()}

	output, err := exec.Command(cmdName, cmdArgs...).Output()
	if err != nil {
		log.Printf("Error executing command: %s\n", err)
		return "", err
	}

	log.Printf("%s\n", output)
	return fmt.Sprintf("%s", output), nil
}

func deleteResource(name string, resource string, namespace string) (string, error) {
	// TODO This could be enhanced a bit to look to see if the item was there to begin with

	cmdName := getKubectl()
	cmdArgs := []string{"delete", resource + "/" + name, "--namespace", namespace}

	output, err := exec.Command(cmdName, cmdArgs...).Output()
	if err != nil {
		log.Printf("Error executing command: %s\n", err)
		return "", err
	}

	log.Printf("%s\n", output)
	return fmt.Sprintf("%s", output), nil
}
