package listers

import (
	kubelessclt "github.com/kubeless/kubeless/pkg/client/clientset/versioned"
	"gitlab.com/mvenezia/kless-api/pkg/util/k8sutil"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
)

var ListWatchers map[string]*cache.ListWatch

func StartListWatchers() {
	kubernetesClient, err := kubernetes.NewForConfig(k8sutil.DefaultConfig)
	if err != nil {
		panic("Cannot make standard kubernetes client")
	}
	kubelessClient, err := kubelessclt.NewForConfig(k8sutil.DefaultConfig)
	if err != nil {
		panic("Cannot make kubeless kubernetes client")
	}
	ListWatchers = make(map[string]*cache.ListWatch)
	ListWatchers["pods"] = cache.NewListWatchFromClient(kubernetesClient.CoreV1().RESTClient(), "pods", "kubeless", fields.Everything())
	ListWatchers["functions"] = cache.NewListWatchFromClient(kubelessClient.KubelessV1beta1().RESTClient(), "pods", "kubeless", fields.Everything())
}
